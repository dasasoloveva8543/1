# 1


## Описание репозитория

Репозиторий для отработки команд Git

## Используемые команды:

- [ ] [Установка](https://git-scm.com/download/win)
- [ ] Настройка
```
git config --global user.name "Соловьева"
git config --global user.email test@example.com
```
- [ ] Создание локального репозитория git_project
- [ ] Определение состояния
```
git status
```
- [ ] Подготовка файлов
```
git add test.txt
```
- [ ] Фиксация изменений
```
git commit -m 'first commit'
```
- [ ] Создание и подключение к удаленному репозиторию
```
git remote add origin https://gitlab.com/dasasoloveva8543/1
``` 
- [ ] Отправка изменений на сервер
```
git push origin master
```
- [ ] Запрос изменений с сервера 
```
git pull origin master
```
- [ ] Ветвление 
```
git branch ветка1
git checkout ветка1
git add дляветки1.txt
git commit -m "мой файл для ветки 1"
git merge ветка1
git branch -d ветка1

```
- [ ] SourceTree
```
SourceTree
```
